// Copyright Epic Games, Inc. All Rights Reserved.

#include "WinterRunnerCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "WinterRunner/Game/WinterRunnerGameInstance.h"

// AWinterRunnerCharacter
AWinterRunnerCharacter::AWinterRunnerCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}


void AWinterRunnerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// update character location
	MovementTick(DeltaSeconds);
}


void AWinterRunnerCharacter::BeginPlay()
{
	Super::BeginPlay();

	// setup character speed
	UWinterRunnerGameInstance* GameInst = Cast<UWinterRunnerGameInstance>(GetGameInstance());
	if (IsValid(GameInst)) {
		BaseSpeed = GameInst->MovementSpeedMin;
		MaxSpeed = GameInst->MovementSpeedMax;
		ShiftOffset = GameInst->LineShiftOffset;
		DistanceToSpeedIncrease = GameInst->DistanceToSpeedIncrease;
	}
	SetCharacterSpeed(BaseSpeed);
}


void AWinterRunnerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("ShiftLeft"), IE_Pressed, this, &AWinterRunnerCharacter::ShiftLineToLeft);
	PlayerInputComponent->BindAction(TEXT("ShiftRight"), IE_Pressed, this, &AWinterRunnerCharacter::ShiftLineToRight);
}

void AWinterRunnerCharacter::MovementTick(float DeltaTime)
{
	
	if (bGameRunning) {
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f)); // move along x axis 
		// increase speed by distance
		float CurrentSpeed = GetCharacterSpeed();

		CoveredDistance += DeltaTime * CurrentSpeed;
		DistanceToSpeedIncrease -= DeltaTime * CurrentSpeed;
		if ((DistanceToSpeedIncrease <= 0.0f) && (CurrentSpeed <= MaxSpeed)) {
			// set new character speed			
			SetCharacterSpeed(GetCharacterSpeed() * 1.2);
			
			UWinterRunnerGameInstance* GameInst = Cast<UWinterRunnerGameInstance>(GetGameInstance());
			if (IsValid(GameInst))
				DistanceToSpeedIncrease = GameInst->DistanceToSpeedIncrease;
			else
				DistanceToSpeedIncrease = 5000.0f;
		}

		UpdatePlayerScore(static_cast<int32>(CoveredDistance) / 1000);
	}
}

void AWinterRunnerCharacter::ChangeMovementLine(bool bToRight)
{
	if (bGameRunning) {
		if (bToRight) {
			if (CurrentLine != EMovementLine::RIGHT) {
				SetActorLocation(GetActorLocation() + FVector(0.0f, ShiftOffset, 0.0f));
				CurrentLine = (CurrentLine == EMovementLine::LEFT) ? EMovementLine::MID : EMovementLine::RIGHT;
			}
		}
		else {
			if (CurrentLine != EMovementLine::LEFT) {
				SetActorLocation(GetActorLocation() + FVector(0.0f, -ShiftOffset, 0.0f));
				CurrentLine = (CurrentLine == EMovementLine::RIGHT) ? EMovementLine::MID : EMovementLine::LEFT;
			}
		}
	}
}

void AWinterRunnerCharacter::SetMovementState(bool bGameRun)
{
	bGameRunning = bGameRun;
}

void AWinterRunnerCharacter::AddPickupBonus(int32 BonusVal)
{
	UpdatePlayerScore(BonusVal);
	OnPlayerTakeBonus.Broadcast(BonusVal);
}

void AWinterRunnerCharacter::UpdatePlayerScore(int32 AddedValue)
{
	PlayerScore += AddedValue;
	OnPlayerStatChanged.Broadcast(PlayerScore);
}

float AWinterRunnerCharacter::GetCharacterSpeed() const
{
	return GetCharacterMovement()->MaxWalkSpeed;
}

void AWinterRunnerCharacter::SetCharacterSpeed(float NewSpeed)
{
	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;
}

int32 AWinterRunnerCharacter::GetPlayerScore() const
{
	return PlayerScore;
}

void AWinterRunnerCharacter::ShiftLineToLeft()
{
	bool bShiftRight = false;
	ChangeMovementLine(bShiftRight);
}

void AWinterRunnerCharacter::ShiftLineToRight()
{
	bool bShiftRight = true;
	ChangeMovementLine(bShiftRight);
}
