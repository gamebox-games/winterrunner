// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "WinterRunnerCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerStatChanged, int32, CurrentScore);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerTakeBonus, int32, BonusValue);

UENUM(BlueprintType)
enum class EMovementLine : uint8
{
	LEFT,
	MID,
	RIGHT
};

UCLASS(config=Game)
class AWinterRunnerCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Stats")
		FOnPlayerStatChanged OnPlayerStatChanged;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Stats")
		FOnPlayerTakeBonus OnPlayerTakeBonus;

	AWinterRunnerCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	// Start function
	UFUNCTION()
		void BeginPlay();

protected:
	// APawn inputs
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

public:
	// Tick movement function
	UFUNCTION()
		void MovementTick(float DeltaTime);

	// Pawm movement
	UFUNCTION(BlueprintCallable)
		void ChangeMovementLine(bool bToRight);
	UFUNCTION()
		void ShiftLineToLeft();
	UFUNCTION()
		void ShiftLineToRight();

	// Pawn speed
	UFUNCTION()
		float GetCharacterSpeed() const;
	UFUNCTION()
		void SetCharacterSpeed(float NewSpeed);
	UFUNCTION(BlueprintCallable)
		void SetMovementState(bool bGameRun);

	// Pawn interface
	UFUNCTION(BlueprintCallable)
		void AddPickupBonus(int32 BonusVal);
	UFUNCTION(BlueprintCallable)
		void UpdatePlayerScore(int32 AddedValue);
	UFUNCTION(BlueprintCallable)
		int32 GetPlayerScore() const;

protected:
	float BaseSpeed = 300.0f;
	float MaxSpeed = 1000.0f;
	float ShiftOffset = 300.0f;
	EMovementLine CurrentLine = EMovementLine::MID;

	float CoveredDistance = 0.0f;
	float DistanceToSpeedIncrease = 5000.f;

	// enable or disabling movement
	bool bGameRunning = false;

	int32 PlayerScore = 0;
};

