// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupBase.h"

// Sets default values
APickupBase::APickupBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create scene component
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	// create skeletal mesh
	PickupSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	PickupSkeletalMesh->SetGenerateOverlapEvents(true);
	PickupSkeletalMesh->SetCollisionProfileName(TEXT("OverlapAll"));
	PickupSkeletalMesh->SetupAttachment(RootComponent);

	// create static mesh
	PickupStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	PickupStaticMesh->SetGenerateOverlapEvents(true);
	PickupStaticMesh->SetCollisionProfileName(TEXT("OverlapAll"));
	PickupStaticMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APickupBase::BeginPlay()
{
	Super::BeginPlay();
	
	// use either static or skeletal mesh
	if (PickupStaticMesh && PickupStaticMesh->GetStaticMesh()) {
		PickupSkeletalMesh->DestroyComponent();
	}
	else if (PickupSkeletalMesh && PickupSkeletalMesh->SkeletalMesh)
	{
		PickupStaticMesh->DestroyComponent();
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("APickupBase::BeginPlay - No static or skeletal meshes provided"));
	}
}

// Called every frame
void APickupBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// add some rotaion by z axis 
	FRotator Rotation = GetActorRotation() + FRotator(0.0f, DeltaTime*100.f, 0.0f);
	UpdateTransform(FTransform((Rotation), GetActorLocation(), FVector(1.0f, 1.0f, 1.0f)));
}

void APickupBase::UpdateTransform(FTransform Offset)
{
	SetActorTransform(Offset);
}

int32 APickupBase::GetBonusPoints() const
{
	return BonusValue;
}

