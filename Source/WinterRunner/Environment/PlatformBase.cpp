// Fill out your copyright notice in the Description page of Project Settings.


#include "WinterRunner/Environment/PlatformBase.h"
#include "Components/BoxComponent.h"
#include "Engine/StaticMeshActor.h"
#include "WinterRunner/Character/WinterRunnerCharacter.h"
#include "WinterRunner/Game/WinterRunnerGameInstance.h"


// Sets default values
APlatformBase::APlatformBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create scene component
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = SceneComponent;

	StaticMeshFloor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshFloor->SetCollisionProfileName(TEXT("BlockAll"));
	StaticMeshFloor->SetupAttachment(RootComponent);

	FloorEndForwardVector = CreateDefaultSubobject<UArrowComponent>(TEXT("End Forward Vector"));
	FloorEndForwardVector->SetupAttachment(RootComponent);

	FloorStartForwardVector = CreateDefaultSubobject<UArrowComponent>(TEXT("Start Forward Vector"));
	FloorStartForwardVector->SetupAttachment(RootComponent);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APlatformBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlatformBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

