// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "WinterRunnerGameInstance.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGameRun, bool, bGameRunning);

/**
 * 
 */
UCLASS()
class WINTERRUNNER_API UWinterRunnerGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	//FOnGameRun OnGameRun;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MovementSpeedMin = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MovementSpeedMax = 2000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float DistanceToSpeedIncrease = 5000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float LineShiftOffset = 300.0f;
};
