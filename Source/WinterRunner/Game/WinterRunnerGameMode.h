// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "WinterRunnerGameMode.generated.h"

UCLASS(minimalapi)
class AWinterRunnerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AWinterRunnerGameMode();
};



