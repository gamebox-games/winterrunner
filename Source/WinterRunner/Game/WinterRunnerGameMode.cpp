// Copyright Epic Games, Inc. All Rights Reserved.

#include "WinterRunnerGameMode.h"
#include "WinterRunner/Character/WinterRunnerCharacter.h"
#include "UObject/ConstructorHelpers.h"

AWinterRunnerGameMode::AWinterRunnerGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/WinterRunner/Characters/BP_Character"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
