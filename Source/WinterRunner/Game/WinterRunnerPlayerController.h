// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "WinterRunnerPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class WINTERRUNNER_API AWinterRunnerPlayerController : public APlayerController
{
	GENERATED_BODY()
	
};
